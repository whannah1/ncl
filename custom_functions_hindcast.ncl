load "$NCARG_ROOT/constants.ncl"
HCdatadir = "~/Data/DYNAMO/Hindcast/"
lat1 = new(1,float)
lat2 = new(1,float)
lon1 = new(1,float)
lon2 = new(1,float)
lat1 = -90.
lat2 =  90.
lon1 =   0.
lon2 = 360.
;===================================================================================
;===================================================================================
procedure HC_opt (opt [1]:logical)
local files,f,num_f,V
begin
	if isatt(opt,"lat1") then lat1 = opt@lat1 else lat1 = -90. end if
  	if isatt(opt,"lat2") then lat2 = opt@lat2 else lat2 =  90. end if
  	if isatt(opt,"lon1") then lon1 = opt@lon1 else lon1 =   0. end if
  	if isatt(opt,"lon2") then lon2 = opt@lon2 else lon2 = 360. end if
end
;===================================================================================
;===================================================================================
function LoadHClat (case,opt [1]:logical)
local files,f,num_f,V
begin
    HC_opt(opt)
    files = systemfunc("ls "+HCdatadir+"raw/"+case+"/"+case+".cam2.h1.*")
    infile = addfile(files(0),"r")
    lat = infile->lat({opt@lat1:opt@lat2})
    return lat
end
;===================================================================================
;===================================================================================
function LoadHClon (case,opt [1]:logical)
local files,f,num_f,V
begin
    HC_opt(opt)
    files = systemfunc("ls "+HCdatadir+"raw/"+case+"/"+case+".cam2.h1.*")
    infile = addfile(files(0),"r")
    lon = infile->lon({opt@lon1:opt@lon2})
    return lon
end
;===================================================================================
;===================================================================================
function LoadHC (case,var,lev,opt [1]:logical)
local files,f,num_f,V
begin
    files = systemfunc("ls "+HCdatadir+"raw/"+case+"/"+case+".cam2.h1.*")
    num_f = dimsizes(files)-1
    do f = 0,num_f-1
    infile = addfile(files(f),"r")
    if f.eq.0 then
        lat = infile->lat({opt@lat1:opt@lat2})
        lon = infile->lon({opt@lon1:opt@lon2})
        ;num_lev = dimsizes(lev)
        num_lat = dimsizes(lat)
        num_lon = dimsizes(lon)
        num_t = 4*num_f
        V = new((/num_t,num_lat,num_lon/),float)
        V!0 = "time"
        V!1 = "lat"
        V!2 = "lon"
        V&lat = lat
        V&lon = lon
    end if
    t1 = f*4
    t2 = t1+dimsizes(infile->time)-1
    if var.eq."PRECT" then
        V(t1:t2,:,:) = infile->PRECC(:,{opt@lat1:opt@lat2},{opt@lon1:opt@lon2}) + infile->PRECL(:,{opt@lat1:opt@lat2},{opt@lon1:opt@lon2})
    else
      if dimsizes(dimsizes(infile->$var$)).eq.3 then
        V(t1:t2,:,:) = infile->$var$(:,{opt@lat1:opt@lat2},{opt@lon1:opt@lon2})
      else
        V(t1:t2,:,:) = dim_avg_n( vinth2p(infile->$var$(:,:,{opt@lat1:opt@lat2},{opt@lon1:opt@lon2}), \
                 infile->hyam,infile->hybm,lev,infile->PS(:,{opt@lat1:opt@lat2},{opt@lon1:opt@lon2}),1,1000.,1,False) ,1) 
      end if
    end if
    end do
    return(V)
end
;===================================================================================
;===================================================================================
function LoadHC4D (case,var,lev,opt [1]:logical)
local files,f,num_f,V
begin
  HC_opt(opt)
  files = systemfunc("ls "+HCdatadir+"raw/"+case+"/"+case+".cam2.h1.*")
  num_f = dimsizes(files)-1
  do f = 0,num_f-1
    infile = addfile(files(f),"r")
    if f.eq.0 then
      lat = infile->lat({lat1:lat2})
      lon = infile->lon({lon1:lon2})
      num_lev = dimsizes(lev)
      num_lat = dimsizes(lat)
      num_lon = dimsizes(lon)
      num_t = 4*num_f
      V = new((/num_t,num_lev,num_lat,num_lon/),float)
        V!0 = "time"
        V!1 = "lev"
        V!2 = "lat"
        V!3 = "lon"
        V&lev = lev
        V&lat = lat
        V&lon = lon
    end if
    t1 = f*4
    t2 = t1+dimsizes(infile->time)-1
    V(t1:t2,:,:,:) = (/ vinth2p(infile->$var$(:,:,{lat1:lat2},{lon1:lon2}), \
             infile->hyam,infile->hybm,lev,infile->PS(:,{lat1:lat2},{lon1:lon2}),1,1000.,1,False) /)
  end do
  return(V)
end
;===================================================================================
;===================================================================================
function LoadHCQR (case,var,lev,opt [1]:logical)
local QRL,QRS
begin
    QRL = LoadHC4D(case,"QRL",lev,opt)
    QRS = LoadHC4D(case,"QRS",lev,opt)
    QR = QRL+QRS
    return QR
end
;===================================================================================
;===================================================================================
