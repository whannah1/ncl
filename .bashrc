# .bashrc
#---------------------------------------------------------------#
#---------------------------------------------------------------#
# don't put duplicate lines in the history. See bash(1) for more options
export HISTCONTROL=ignoredups

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

#---------------------------------------------------------------#
# Set OS and machine name
#---------------------------------------------------------------#
export os=`uname -s`
export host=`hostname | cut -d. -f1`


if [[ $host == *"yslogin"* ]]; then
    host='yellowstone'
fi

if [[ $host == *"blogin"* ]]; then
    host='anvil'
fi

if [[ $host == *"titan"* ]]; then
    host='titan'
fi

if [ -f /etc/bashrc ]; then
    source /etc/bashrc
fi

#if [ $host = 'anvil' ]; then 
#    source /etc/bashrc
#fi
#---------------------------------------------------------------#
# Terminal Colors
#---------------------------------------------------------------#
if [ $os = 'Darwin' ]; then
    export CLICOLOR=1
    export LS_COLORS=HxfxcxdxaxegedabagExEx     # used for the terminal app
    export TERM=xterm-color                     # used for iterm
fi
    #LSCOLORS codes
    #a  black            #A   bold black (dark grey)
    #b  red              #B   bold red
    #c  green            #C   bold green
    #d  brown            #D   bold brown (yellow)
    #e  blue             #E   bold blue
    #f  magenta          #F   bold magenta
    #g  cyan             #G   bold cyan
    #h  light grey       #H   bold light grey (bright white)
    #x  default FG or BG

    #1. directory        #7.  character special
    #2. symbolic link    #8.  executable with setuid bit set
    #3. socket           #9.  executable with setgid bit set
    #4. pipe             #10. directory writable to others, with sticky bit
    #5. executable       #11. directory writable to others, without sticky
    #6. block special
    
#---------------------------------------------------------------#
# Set custom prompt    
#---------------------------------------------------------------#
    #PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\[\033[00m\]\>'
    case $os in
        "Linux"  )
            case "$TERM" in
                xterm-color)
                    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\> ';;
                *)
                    PS1='${HOSTNAME}:\w> ';;
            esac ;;
        "Darwin" )
            # PS1='$host:\w> ' ;;
            PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\> ';;
    esac

    # if [ $host = 'anvil' ]; then 
    #     PS1='${HOSTNAME}:\w> '
    # fi
#---------------------------------------------------------------#
# Aliases
#---------------------------------------------------------------#
    case $os in
        "Darwin" )
            alias ls='ls -Gh'
            alias btfix='sudo defaults write com.apple.BluetoothAudioAgent "Apple Bitpool Min (editable)" 53';;
        "Linux"  )
            alias ls='ls --color=auto';;
    esac

    # if [ $host = 'anvil' ]; then 
    #     alias ls='ls'
    # fi

    alias ll='ls -lh'
    alias cd..="cd .."
    alias ll='ls -lh'
    alias h=history
    alias lsout='ls -lt *out | head '
    alias du='du -shc'
    alias dud='du -shc ./*'
    alias gv='gwenview'

    #alias ncl='/NCL/bin/ncl'
    alias WRAPIT='~/NCL/bin/WRAPIT ' #-fPIC 

    alias initialize_git='git init; git add ./*ncl; git add ./*py; git add ./*/*ncl; git commit -m "first commit" '

    # Aliases for checking on running processes
    alias psncl='ps -fC ncl'
    alias  pspy='ps -fC python'
    alias psmpi='ps -fC mpirun'

    alias qjob='qstat -u whannah,hannah6 ; qstat | grep "hannah" '

    if [ $host == 'scylla' ]; then 
        alias scratch='cd ~/Data/Model/CESM/scratch'
    fi

    alias samlog='ls /home/whannah/SAM/SAM6.10.10/logs/* -lth | head -n 10'
#---------------------------------------------------------------#
# Remote SSH Connection Aliases
#---------------------------------------------------------------#
# Personal computers
    #alias abalone='ssh -Y whannah@abalone.local'
    alias abalone='ssh -Y whannah@abalone.duckdns.org'
    #alias abalone='ssh 76.110.204.80'
    # alias scylla='ssh -Y whannah@scylla.meas.ncsu.edu'
    # alias scyllafs='sshfs whannah@scylla.meas.ncsu.edu:/home/whannah/ ~/scylla/'
    # alias scylla='ssh -Y  whannah@73.202.141.117'
    # alias scyllafs='sshfs whannah@73.202.141.117 ~/scylla/'
    alias scylla='ssh -Y  whannah@scylla-home.duckdns.org'
    alias scyllafs='sshfs whannah@scylla-home.duckdns.org:/home/whannah/ ~/scylla/'

# National Lab Machines
    alias edison='ssh -Y whannah@edison.nersc.gov'
    alias anvil=' ssh -Y whannah@blues.lcrc.anl.gov'
    alias titan=' ssh -Y hannah6@titan.ccs.ornl.gov'hannah6
    alias aims4=' ssh -Y hannah6@aims4.llnl.gov'
    alias cab=' ssh -Y hannah6@cab.llnl.gov'
    alias edisonfs='sshfs whannah@edison.nersc.gov:/global/homes/w/whannah/   ~/edison/'
    alias anvilfs=' sshfs whannah@blues.lcrc.anl.gov:/home/whannah/           ~/anvil/'
    alias titanfs=' sshfs hannah6@titan.ccs.ornl.gov:/ccs/home//       ~/titan/'
    

# NCAR servers
    alias mirage='ssh -Y whannah@mirage2.ucar.edu'
    alias bluefire='ssh -Y whannah@bluefire.ucar.edu'
    alias yellowstone='ssh -Y whannah@yellowstone.ucar.edu'
    alias yellowstonefs='sshfs whannah@yellowstone.ucar.edu:/glade/u/home/whannah/ ~/yellowstone'

# NCSU servers
    alias monsoon='ssh -X whannah@192.168.100.75'
    alias typhoon='ssh -X whannah@192.168.100.68'
    alias teak='ssh -X whannah@192.168.100.70'
    alias lotus='ssh -X whannah@192.168.100.69'
    alias banyan='ssh -X whannah@192.168.100.66'

# Miami Servers
    alias vis='ssh -Y whannah@vis.ccs.miami.edu'
    alias visx='ssh -X whannah@visx.ccs.miami.edu'
    alias weather='ssh -Y whannah@weather.rsmas.miami.edu'
    alias pegasus='ssh -Y whannah@pegasus2.ccs.miami.edu'
    #alias pegfs='sshfs whannah@pegasus2.ccs.miami.edu:~/ ~/Research/pegasus/'
    alias pegfs='sshfs whannah@pegasus2.ccs.miami.edu:/nethome/whannah/ ~/Research/pegasus/'
    alias echidna='ssh -Y whannah@echidna.rsmas.miami.edu'
    #alias echidna='ssh -Y 129.171.98.205'

# CSU servers
    alias keto='ssh -Y whannah@keto.atmos.colostate.edu'
    alias mare='ssh -Y whannah@mare.atmos.colostate.edu'
    
# UNIST Servers (Korea)
    alias poseidon='ssh -Y walter@10.20.22.81'
    alias poseidonout='ssh -Y walter@114.70.9.42'
    alias poseidonftp='sftp walter@10.20.22.81'
    alias poseidonftpout='sftp walter@114.70.9.42'
    alias zeus='ssh -Y walter@10.20.22.80'
    alias kraken='ssh -Y whannah@kraken.nics.utk.edu'

# Bluehost web hosting account for HannahLab.org
    alias bluehost='ssh walterha@hannahlab.org'
#---------------------------------------------------------------#
# PATH                 
#---------------------------------------------------------------#
    export MAGICK_HOME=/Users/whannah/ImageMagick-6.7.5
    
    export PYTHONPATH=$PYTHONPATH:~/ECMWF

    export NETCDF_C_DIR=~/NetCDF/bld-netcdf-c-4.3.2
    export NETCDF_F_DIR=~/NetCDF/bld-netcdf-fortran-4.4.1
    
    if [ $host != 'yellowstone' ]; then
    case $os in
        "Darwin" )
            export PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/texbin;;
        "Linux" )
            export PATH=$PATH:/usr/lib64/:/lib64/:/usr/lib64/qt-3.3/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin;;
    esac
    fi

    if [ $os = 'Darwin' ]; then
        export PATH=${PATH}:$MAGICK_HOME/bin
        #export PATH=${PATH}:/usr/local/bin
        #export PATH=${PATH}:/usr/X11
        export PATH=${PATH}:/usr/local/pgsql/bin
        export PATH=${PATH}:/Applications/nedit
        export PATH=${PATH}:/Applications/Sublime_Text_2.app/Contents/SharedSupport/bin
        export PATH=/usr/local/bin:usr/local/lib:${PATH}
        export PATH=~/anaconda/bin:$PATH
        export PATH=~/NetCDF/bld-netcdf-c-4.3.2/bin:$PATH
        export PATH=~/NetCDF/bld-netcdf-fortran-4.4.1/bin:$PATH
    fi

    if [ $host == 'scylla' ]; then
        export NETCDF_DIR=/usr/local/netcdf
        export MPI_DIR=/usr/local/openmpi
        #export PATH=~/anaconda/bin:$PATH
        export PATH=$MPI_DIR/bin:$PATH
        export PATH=$NETCDF_DIR/bin:$PATH
        export PATH=/usr/local/ncview/bin:$PATH
        export PATH=/usr/local/cdo/bin:$PATH
    fi
    
    if [ $host == 'monsoon' ]; then
        PATH=$PATH:$HOME/bin
        export NETCDF_DIR=/share/apps/netcdf
        export PATH=$NETCDF_DIR/bin:$PATH
        #export MPI_DIR=/usr/local/openmpi
        #export PATH=$MPI_DIR/bin:$PATH
        #export PATH=/usr/local/ncview/bin:$PATH
        #export PATH=/usr/local/cdo/bin:$PATH
        export PATH=$PATH:/opt/rocks/bin
    fi

    if [ $host = 'weather' ]; then
        #export PATH=/usr/lib64/:/lib64/:/usr/lib64/qt-3.3/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin
        export PATH=$PATH:/home/whannah/NCL/bin
        export PATH=/home/whannah/NetCDF/bld-netcdf-c-4.3.2/bin/:$PATH
        #export PATH=/home/whannah/NetCDF/bld-ncview-2.1.2/bin/:$PATH

        export LIB_PATH=/home/whannah/local/
        export PATH=$LIB_PATH/bld-curl-7.26.0/:$PATH
        export PATH=$LIB_PATH/bld-zlib-1.2.8/:$PATH
        export PATH=$LIB_PATH/bld-hdf5-1.8.13/:$PATH
        export PATH=$LIB_PATH/libXaw/:$PATH
        export PATH=$LIB_PATH/libXaw/usr/:$PATH
        export PATH=~/anaconda/bin:$PATH
    fi
    
    #if [ $host = 'yellowstone' ]; then
    #    export PATH=/glade/apps/opt/ncview/2.1.1/gnu/4.4.6/bin
    #    export PATH=$PATH:/usr/lib64/qt-3.3/bin
    #    export PATH=$PATH:/glade/apps/opt/netcdf/4.3.0/intel/12.1.5/bin
    #    export PATH=$PATH:/glade/apps/opt/modulefiles/ys/cmpwrappers
    #    export PATH=$PATH:/ncar/opt/intel/12.1.0.233/composer_xe_2011_sp1.11.339/bin/intel64
    #    export PATH=$PATH:/glade/apps/opt/usr/bin
    #    export PATH=$PATH:/ncar/opt/lsf/9.1/linux2.6-glibc2.3-x86_64/etc
    #    export PATH=$PATH:/ncar/opt/lsf/9.1/linux2.6-glibc2.3-x86_64/bin
    #    export PATH=$PATH:/usr/bin:/bin:/usr/sbin:/sbin
    #    export PATH=$PATH:/usr/local/openssh/5.7p1krb/bin
    #    export PATH=$PATH:/usr/local/sbin:/opt/ibutils/bin
    #    export PATH=$PATH:/ncar/opt/hpss
    #    export PATH=$PATH:/glade/apps/opt/ncl/6.2.0/intel/12.1.5/bin
    #fi
#---------------------------------------------------------------
# Modules
#---------------------------------------------------------------
    if [ $host = 'yellowstone' ]; then
        #module load ncl
        module load ncview
    fi

    if [ $host = 'anvil' ]; then 
        resoft
    fi

    if [ $host = 'titan' ]; then 
        module load python
        module load python_numpy
        # module load cray-netcdf
        module load cray-mpich

        # module load cray-mpich/7.5.2
        module load cray-netcdf-hdf5parallel
        
        # module load mpip
	    module load ncview
        export PATH=$PATH:/ccs/home/hannah6/nco/bin
    fi
#---------------------------------------------------------------
# NCL
#---------------------------------------------------------------
    export NCARG_ROOT=~/NCL
    export PATH=${PATH}:$NCARG_ROOT/bin
    #if [ $os = 'Darwin' ]; then
        #export DYLD_LIBRARY_PATH="" #"/usr/local/lib/"
        #export DYLD_FALLBACK_LIBRARY_PATH="/usr/local/lib/"
    #   export DYLD_FALLBACK_LIBRARY_PATH="/usr/local/lib/gcc/x86_64-apple-darwin13.2.0/4.8.3"
    #   if [ $host = 'nereus' ]; then
    #       export DYLD_FALLBACK_LIBRARY_PATH="/usr/local/lib/gcc/4.9/gcc/x86_64-apple-darwin13.4.0/4.9.2/"
    #   fi
    #fi

    #if [ $host = 'yellowstone' ]; then
    #    export NCARG_ROOT='/glade/u/apps/opt/ncl/6.1.0/gnu/4.4.6'
    #fi

    # The following links are helpful for getting around errors such as 
    # dyld: Library not loaded: /usr/local/lib/libgfortran.3.dylib
    # dyld: Library not loaded: /usr/local/lib/libquadmath.0.dylib
    # dyld: Library not loaded: /usr/local/lib/libgomp.1.dylib
    # ln -s /usr/local/Cellar/gcc/6.3.0_1/lib/gcc/6/libgfortran.3.dylib /usr/local/lib/libgfortran.3.dylib
    # ln -s  /usr/local/Cellar/gcc/6.3.0_1/lib/gcc/6/libquadmath.0.dylib /usr/local/lib/libquadmath.0.dylib
    # ln -s /usr/local/Cellar/gcc/6.3.0_1/lib/gcc/6/libgomp.1.dylib /usr/local/lib/libgomp.1.dylib
#---------------------------------------------------------------#
# X11 
#---------------------------------------------------------------#
    #export DISPLAY=:0.0

    #if [ $os = 'Darwin' ]; then
    #    dispdir=`dirname $DISPLAY`
    #    dispfile=`basename $DISPLAY`
    #    dispnew="$dispdir/:0"
    #    if [ -e $DISPLAY -a "$dispfile" = "org.x:0" ]; then
    #    mv $DISPLAY $dispnew
    #    fi
    #    export DISPLAY=$dispnew
    #fi

    if [ $os = 'Darwin' ]; then
    if [ $host = 'abalone' ]; then
        #PATH=${PATH}:/opt/X11R6/bin
        PATH=${PATH}:/usr/X11R6/bin
    fi
    fi

#---------------------------------------------------------------#
# Vapor
#---------------------------------------------------------------#
    #. /Applications/VAPOR.app/Contents/MacOS/vapor-setup.sh
    
    #if [ $host = 'scylla' ]; then
    #   export VAPOR_HOME='/usr/local/vapor-2.5.0'
    #   . $VAPOR_HOME/bin/vapor-setup.sh
    #fi


#---------------------------------------------------------------#
# HDF5
#---------------------------------------------------------------#
if [ $os = 'Linux' ]; then

    if [ $host = 'weather' ]; then
        export CUR_HDF5=$LIB_PATH/bld-hdf5-1.8.13/
        export CPPFLAGS=-I$CUR_HDF5/include
        export LDFLAGS=-L$CUR_HDF5/lib
        export LD_LIBRARY_PATH=$CUR_HDF5/lib
    fi

    if [ $host = 'scylla' ]; then
       #export LDFLAGS="-L/usr/lib64 "
       #export LD_LIBRARY_PATH=/home/MPI/openmpi/lib64:$NETCDF_C_DIR/lib64:$NETCDF_F_DIR/lib64:/usr/lib64
       export LD_LIBRARY_PATH=$NETCDF_DIR/lib:$MPI_DIR/lib
    fi
fi


