;======================================================================================
;======================================================================================
; This library contains several specialized NCL functions/procedures for 
; loading coordinate data from model output from the System for Atmospheric Modelling (SAM)
;
;   Walter Hannah       North Carolina State University
;
;   loadSAMx    (case)      Load X coordinate
;   loadSAMy    (case)      Load Y coordinate
;   loadSAMz    (case)      Load Z coordinate
;   loadSAMtime (case)      Load time coordinate
;   loadSAMavgp (case)      Load 2D pressure (time,z)
;   loadSAMp    (case)      Load 4D pressure w/ perturbations (time,z,y,x)
;======================================================================================
;======================================================================================
function loadSAMx (case)
local files,ifile,infile
begin
    files = systemfunc("ls "+SAM_data_dir+case+"/OUT_2D/"+case+"*.nc")
    ifile = files(0)
    infile = addfile(ifile,"r")
    return infile->x
end
;======================================================================================
;======================================================================================
function loadSAMy (case)
local files,ifile,infile
begin
    files = systemfunc("ls "+SAM_data_dir+case+"/OUT_2D/"+case+"*.nc")
    ifile = files(0)
    infile = addfile(ifile,"r")
    return infile->y
end
;======================================================================================
;======================================================================================
function loadSAMz (case)
local files,ifile,infile
begin
    ifile = systemfunc("ls "+SAM_data_dir+case+"/OUT_STAT/"+case+".nc")
    infile = addfile(ifile,"r")
    return infile->z
end
;======================================================================================
;======================================================================================
function loadSAMtime (case)
local files,ifile,infile
begin
  ifile = systemfunc("ls "+SAM_data_dir+case+"/OUT_STAT/"+case+".nc")
  infile = addfile(ifile,"r")
  return infile->time
end
;======================================================================================
;======================================================================================
function loadSAMavgp (case)
local files,ifile,infile
begin
    ifile = systemfunc("ls "+SAM_data_dir+case+"/OUT_STAT/"+case+".nc")
    infile = addfile(ifile,"r")
    return infile->p
end
;======================================================================================
;======================================================================================
function loadSAMp (case,t)
local files,ifile,infile
begin
    files = systemfunc("ls "+SAM_data_dir+case+"/OUT_3D/"+case+"*.nc")
    ifile  = files(t)
    infile = addfile(ifile,"r")
    pp = infile->PP(0,:,:,:)
    delete(files)
    ifile = systemfunc("ls "+SAM_data_dir+case+"/OUT_STAT/"+case+".nc")
    infile = addfile(ifile,"r")
    p  = infile->p
    P  = pp + conform(pp,p,0)
    P!0 = "z"
    P!1 = "y"
    P!2 = "x"
    P&z = infile->z
    return P
end
;======================================================================================
;======================================================================================
